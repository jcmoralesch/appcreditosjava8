
package formulario;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Locale;
//import java.sql.SQLException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;


public class copiaSeguridad extends javax.swing.JInternalFrame {
JFileChooser chooser=new JFileChooser();
    
    public copiaSeguridad() {
        initComponents();
        llenarCombo();
    }

    private void llenarCombo(){
      ConexionFR cc = new ConexionFR();
      Connection cn = cc.Conexion();
      PreparedStatement st;
      ResultSet rs;
      
      try{
           Class.forName("com.mysql.jdbc.Driver");
           cn=DriverManager.getConnection("jdbc:mysql://localhost/creditosinverguate","root","jc109011");
           st=cn.prepareStatement("show databases");
           rs=st.executeQuery();
           
           while(rs.next()){
                this.jComboBox1.addItem(rs.getObject(1));
           }
           
      }
      catch(Exception e){
         JOptionPane.showMessageDialog(null,e.getMessage());
      }
    }
    
    private void crearBackup(String dataBase){
       int resp;
       Calendar c=Calendar.getInstance();
       String fecha=String.valueOf(c.get(Calendar.DATE));
       fecha=fecha+"-"+String.valueOf(c.get(Calendar.MONTH));
       fecha=fecha+"-"+String.valueOf(c.get(Calendar.YEAR));
       
       // aqui mostramos el file chooser para elegir el sitio donde se guarda el backup
       
       resp=chooser.showSaveDialog(this);
       if(resp==JFileChooser.APPROVE_OPTION){
           try{
               Runtime runtime=Runtime.getRuntime();
               File backupFile = new File(String.valueOf(chooser.getSelectedFile().toString())
               +"-"+fecha+".sql");
               FileWriter fw =new FileWriter(backupFile);
               //aqui se ejecuta el comando mysqldump
               //se utiliza el .getproperty() porque se ha copiado el comando mysqldump
               //en la carpeta de la aplicacion..
               Process child= runtime.exec(System.getProperty("user.dir")+
                       "\\mysqldump --opt --password=jc109011 --user=root"
                        +"--databases "+dataBase+" -R");
               
               InputStreamReader irs = new InputStreamReader(child.getInputStream());
               BufferedReader br = new BufferedReader(irs);
               
              String line;
              while((line=br.readLine())!=null){
                     fw.write(line +"\n");
              }
              fw.close();
              irs.close();
              br.close();
           }
           catch(Exception ex){
              JOptionPane.showMessageDialog(null,ex);
           }
           JOptionPane.showMessageDialog(null,"Archivo generado");
       }else if(resp==JFileChooser.CANCEL_OPTION)
       {
         JOptionPane.showMessageDialog(null,"Ha sido cancelado la generacion del Backup");
       }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);

        jButton1.setText("CrearBackup");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(63, Short.MAX_VALUE)
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(63, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(88, 88, 88))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51)
                .addComponent(jButton1)
                .addContainerGap(52, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
          crearBackup(this.jComboBox1.getSelectedItem().toString());        
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox jComboBox1;
    // End of variables declaration//GEN-END:variables

}
