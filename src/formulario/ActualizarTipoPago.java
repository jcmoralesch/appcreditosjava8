
package formulario;

import com.mysql.jdbc.Connection;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;


public class ActualizarTipoPago extends javax.swing.JInternalFrame {
     DefaultTableModel model;
     int contador;
     
  ConexionFR cn = new ConexionFR();
  java.sql.Connection cd = cn.Conexion();
    
    public ActualizarTipoPago() {
        initComponents();
        cargar("");
        limpiar();
        bloquear();
        metodoTipoCredito();
        cargarDatos();
    }
    
     private void cargarDatos(){
         jTable1.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
        
          public void valueChanged(ListSelectionEvent e){
              if(jTable1.getSelectedRow()!=-1){
                    int fila = jTable1.getSelectedRow();
                    txtIdTipo.setText(jTable1.getValueAt(fila,0).toString());
                    txtNombreTi.setText(jTable1.getValueAt(fila,1).toString());
                    txtNumero.setText(jTable1.getValueAt(fila,2).toString());
                    txtCuotas.setText(jTable1.getValueAt(fila,3).toString());
                    txtInteres.setText(jTable1.getValueAt(fila,4).toString());
                    txtCantidad.setText(jTable1.getValueAt(fila,5).toString());
                    txtPlazo.setText(jTable1.getValueAt(fila,6).toString());
                    
              }
       }
       
    });
    }
    
    void limpiar(){
      txtIdTipo.setText("");
      txtNombreTi.setText("");
      txtNumero.setText("");
      txtCuotas.setText("");
      txtInteres.setText("");
      txtCantidad.setText("");
      txtPlazo.setText("");
      txtTipoCredito.setText("");
      
    }
    
    void bloquear(){
      
      txtIdTipo.setEditable(false);
      txtNombreTi.setEditable(false);
      txtNumero.setEditable(false);
      txtCuotas.setEditable(false);
      txtInteres.setEditable(false);
      txtCantidad.setEditable(false);
      txtPlazo.setEditable(false);
      txtTipoCredito.setEditable(false);
           
      btnNuevoTipo.setEnabled(true);
      btnRegisTipo.setEnabled(false);
      btnCancelarTipo.setEnabled(false);
      
    }
    
    
    void desBloquear(){
      txtIdTipo.setEditable(false);
      txtNombreTi.setEditable(true);
      txtNumero.setEditable(true);
      txtCuotas.setEditable(true);
      txtInteres.setEditable(true);
      txtCantidad.setEditable(true);
      txtPlazo.setEditable(true);
      txtTipoCredito.setEditable(true);
      
      btnNuevoTipo.setEnabled(false);
      btnRegisTipo.setEnabled(true);
      btnCancelarTipo.setEnabled(true);
      
    }
    
    void cargar(String valor){
      String [] titulos={"ID. TIPO","NOMBRE","NO. PAGOS","CANTIDAD","CUOTAS A PAGAR","INTERES","PLAZO","CREDITO"}; 
      String [] registros =new String[8];
      
      String sql="SELECT T.id_tipo, T.nombre_tipo,T.no_pago,T.cantidad,T.cuota,T.interes,T.plazo,C.tipo from tipo_pago T,\n" +
"      tipo_credito C where T.id_tipo2=C.id_tipo order by C.tipo";
      model = new DefaultTableModel(null,titulos);
      
 
      
      Statement st;
       try {
           st = cd.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
                while(rs.next()){
                
                       registros[0]=rs.getString("id_tipo");
                       registros[1]=rs.getString("nombre_tipo");
                       registros[2]=rs.getString("no_pago");
                       registros[3]=rs.getString("cantidad");
                       registros[4]=rs.getString("cuota");
                       registros[5]=rs.getString("interes");
                       registros[6]=rs.getString("plazo");
                       registros[7]=rs.getString("tipo");
                       
                      
                       model.addRow(registros);
                }
                
                jTable1.setModel(model);
       } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, ex);
       }
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtNumero = new javax.swing.JTextField();
        txtCuotas = new javax.swing.JTextField();
        txtNombreTi = new javax.swing.JTextField();
        txtIdTipo = new javax.swing.JTextField();
        btnNuevoTipo = new javax.swing.JButton();
        btnRegisTipo = new javax.swing.JButton();
        btnCancelarTipo = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        txtInteres = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        txtCantidad = new javax.swing.JTextField();
        txtPlazo = new javax.swing.JTextField();
        cmbTipo = new javax.swing.JComboBox();
        txtTipoCredito = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);

        jPanel1.setBackground(new java.awt.Color(0, 51, 204));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("ACTUALIZAR TIPO DE PAGO DIARIO Y SEMANAL");

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/recortado2.gif"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(0, 204, 255));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(51, 204, 0)), "ACTUALIZAR DATOS TIPO DE PAGO", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel10.setText("ID. TIPO *");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel11.setText("NOMBRE TIPO");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel12.setText("NUMERO DE PAGOS");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel13.setText("CANTIDAD");

        txtCuotas.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N

        btnNuevoTipo.setBackground(new java.awt.Color(255, 255, 255));
        btnNuevoTipo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnNuevoTipo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/nuevo31.gif"))); // NOI18N
        btnNuevoTipo.setText("Nuevo");
        btnNuevoTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoTipoActionPerformed(evt);
            }
        });

        btnRegisTipo.setBackground(new java.awt.Color(255, 255, 255));
        btnRegisTipo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnRegisTipo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/actualizacionjpg.gif"))); // NOI18N
        btnRegisTipo.setText("Actualizar");
        btnRegisTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisTipoActionPerformed(evt);
            }
        });

        btnCancelarTipo.setBackground(new java.awt.Color(255, 255, 255));
        btnCancelarTipo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnCancelarTipo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/canelar181.gif"))); // NOI18N
        btnCancelarTipo.setText("Cancelar");
        btnCancelarTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarTipoActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel14.setText("CUOTA");

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel24.setText("INTERES");

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel25.setText("PLAZO");

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel26.setText("TIPO DE CREDITO");

        cmbTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel10)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(btnNuevoTipo)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(btnRegisTipo)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(btnCancelarTipo))
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel11)
                                .addComponent(jLabel14)
                                .addComponent(jLabel24)
                                .addComponent(jLabel25))
                            .addGap(36, 36, 36)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtNombreTi, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtPlazo, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtIdTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtInteres, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtCuotas, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(jLabel26)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(txtTipoCredito)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(cmbTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(71, 71, 71)))
                    .addComponent(jLabel13)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtIdTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txtNombreTi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txtCuotas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(txtInteres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(txtPlazo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(cmbTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTipoCredito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNuevoTipo)
                    .addComponent(btnRegisTipo)
                    .addComponent(btnCancelarTipo))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 730, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoTipoActionPerformed
        limpiar();
        desBloquear();
        txtIdTipo.requestFocus();
    }//GEN-LAST:event_btnNuevoTipoActionPerformed

    private void btnRegisTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisTipoActionPerformed

        String idTipo,nombreTipo,cuotas,noPagos,interes,plazo,cantidad,tipoPago;

        idTipo= txtIdTipo.getText();
        nombreTipo=txtNombreTi.getText().toUpperCase();
        noPagos=txtNumero.getText();
        cantidad=txtCantidad.getText();
        cuotas= txtCuotas.getText();
        interes=txtInteres.getText();  
        plazo=txtPlazo.getText();
        tipoPago=txtTipoCredito.getText();

        try{

            CallableStatement statement=(CallableStatement) cd.prepareCall("{call  actualizarTipoPago(?,?,?,?,?,?,?,?)}");
            statement.setString("inIdTipo",idTipo);
            statement.setString("inNombreTipo",nombreTipo);
            statement.setString("inNoPago",noPagos);
             statement.setString("inCantidad",cantidad);
            statement.setString("inCuota",cuotas);
            statement.setString("inInteres",interes);
            statement.setString("inPlazo",plazo);
            statement.setString("inIdTipoCredito",tipoPago);

            int n= statement.executeUpdate();

            if(n>0){
                JOptionPane.showMessageDialog(null,"Datos actualizados correctamente");
                bloquear();
                 cargar("");
            }

        }
        catch(SQLException ex){
            //Logger.getLogger(Ing_Clie.class.getName()).log(Level.SEVERE,sql);
            JOptionPane.showMessageDialog(null,"Error DATOS NO ACTUALIZADOS");
        }
    }//GEN-LAST:event_btnRegisTipoActionPerformed

    private void btnCancelarTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarTipoActionPerformed
        bloquear();
        limpiar();
    }//GEN-LAST:event_btnCancelarTipoActionPerformed

    private void cmbTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipoActionPerformed

        try{
            Statement sent1= cd.createStatement();
            ResultSet rsl = sent1.executeQuery("select * from tipo_credito where  tipo='"+this.cmbTipo.getSelectedItem()+"'");
            rsl.next();
            txtTipoCredito.setText(String.valueOf(rsl.getString("id_Tipo")));
        }
        catch(SQLException e){
            // JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_cmbTipoActionPerformed

       private void metodoTipoCredito(){
    //this.cmbTipo.removeAllItems();
        try{
            
            Statement sent =cd.createStatement();
            ResultSet rs=sent.executeQuery("select * from tipo_credito where tipo='Semanal' or tipo='Diario'");
            while(rs.next()){
                 this.cmbTipo.addItem(rs.getString("tipo"));
            }
              contador++;    
        }catch(SQLException e){
             JOptionPane.showMessageDialog(null,e);
        }
}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelarTipo;
    private javax.swing.JButton btnNuevoTipo;
    private javax.swing.JButton btnRegisTipo;
    private javax.swing.JComboBox cmbTipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtCuotas;
    private javax.swing.JTextField txtIdTipo;
    private javax.swing.JTextField txtInteres;
    private javax.swing.JTextField txtNombreTi;
    private javax.swing.JTextField txtNumero;
    private javax.swing.JTextField txtPlazo;
    private javax.swing.JTextField txtTipoCredito;
    // End of variables declaration//GEN-END:variables
}