package formulario;
import com.mysql.jdbc.CallableStatement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class RegistrarLiquidacion extends javax.swing.JInternalFrame {

     int contador;
     DefaultTableModel model;
     
     ConexionFR cn = new ConexionFR();
     Connection cd = cn.Conexion();
    
    
    public RegistrarLiquidacion() {
        initComponents();
        limpiar();
         cargar("");
       
       lblUsuario.setText(Usuario.nombre);
        
       jTable1.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
        
        
        public void valueChanged(ListSelectionEvent e){
              if(jTable1.getSelectedRow()!=-1){
                    int fila = jTable1.getSelectedRow();
                    txtIdCredito.setText(jTable1.getValueAt(fila,0).toString());
                    txtIdCliente.setText(jTable1.getValueAt(fila,1).toString());
                    txtIdEstadoCuenta.setText(jTable1.getValueAt(fila,16).toString());
              }
       }
    });
    }

    
     void limpiar(){
      txtMora.setText("0.00");
      jTextField1.setText("");
      txtCuota.setText("");
     
     }
       
       void cargar(String valor){
      String [] titulos={"Id. Cred","Identificacion","Nombre","Apellido","Direccion","Telefono","Saldo","Pagos","Cuota","Dias mora","C. acumulada","Mora","Adelantado","T. Adelant.","F. inicio","F. vencimiento","Id.Estado C."}; 
      String [] registros =new String[17];
      
      String sql="SELECT D.id_credito,C.identificacion,C.nombre_cliente,C.ape_cliente,C.direccion, C.telefono,C.status,\n" +
"              E.saldo,E.pagos,D.cuota,M.dias_mora,M.total_de_mora,M.mora_generada,M.adelantado,M.total_adelantado,date_format(D.fecha,'%d/%m/%Y'),date_format(D.fecha_venci,'%d/%m/%Y'),E.id_estado\n" +
"              FROM cliente C,estado_cuenta E,credito_prendario D,mora M \n" +
"              where C.identificacion=E.identificacion and C.identificacion=D.identificacion and C.identificacion=M.identificacion\n" +
"              and D.status='P' and E.status='P' and (E.id_tipo=3 or E.id_tipo=4) and (M.id_tipo=3 or M.id_tipo=4) and CONCAT(nombre_cliente,'',ape_cliente) like'%"+valor+"%'";
      model = new DefaultTableModel(null,titulos);
      
      Statement st;
       try {
           st = cd.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
                while(rs.next()){
                       registros[0]=rs.getString("id_credito");
                       registros[1]=rs.getString("identificacion");
                       registros[2]=rs.getString("nombre_cliente");
                       registros[3]=rs.getString("ape_cliente");
                       registros[4]=rs.getString("direccion");
                       registros[5]=rs.getString("telefono");
                       registros[6]=rs.getString("saldo");
                       registros[7]=rs.getString("pagos");
                       registros[8]=rs.getString("cuota");
                       registros[9]=rs.getString("dias_mora");
                       registros[10]=rs.getString("total_de_mora");
                       registros[11]=rs.getString("mora_generada");
                       registros[12]=rs.getString("adelantado");
                       registros[13]=rs.getString("total_adelantado");
                       registros[14]=rs.getString("date_format(D.fecha,'%d/%m/%Y')");
                       registros[15]=rs.getString("date_format(D.fecha_venci,'%d/%m/%Y')");
                       registros[16]=rs.getString("id_estado");
                      
                       model.addRow(registros);
                }
                
                jTable1.setModel(model);
       } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, ex);
       }
     
  }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblUsuario = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtIdCliente = new javax.swing.JTextField();
        txtCuota = new javax.swing.JTextField();
        btnRegistroDep = new javax.swing.JButton();
        btnCancelarDep = new javax.swing.JButton();
        txtIdCredito = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtIdEstadoCuenta = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtMora = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);

        jPanel1.setBackground(new java.awt.Color(0, 51, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "AREA DE LIQUIDACIÓN", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11), new java.awt.Color(255, 255, 255))); // NOI18N

        lblUsuario.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 536, Short.MAX_VALUE)
                .addComponent(lblUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(lblUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 27, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(0, 204, 255));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(51, 204, 0)), "REGISTRAR LIQUIDACIÓN DE PRENDA", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        jLabel3.setText("ID. CLIENTE *");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("PRECIO VENTA");

        txtIdCliente.setEditable(false);

        txtCuota.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));

        btnRegistroDep.setBackground(new java.awt.Color(255, 255, 255));
        btnRegistroDep.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnRegistroDep.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/canelar199.gif"))); // NOI18N
        btnRegistroDep.setText("Registrar");
        btnRegistroDep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistroDepActionPerformed(evt);
            }
        });

        btnCancelarDep.setBackground(new java.awt.Color(255, 255, 255));
        btnCancelarDep.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnCancelarDep.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/canelar181.gif"))); // NOI18N
        btnCancelarDep.setText("Cancelar");
        btnCancelarDep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarDepActionPerformed(evt);
            }
        });

        txtIdCredito.setEditable(false);
        txtIdCredito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdCreditoActionPerformed(evt);
            }
        });

        jLabel1.setText("Id. credito");

        jLabel8.setText("Id. Estado C.");

        txtIdEstadoCuenta.setEditable(false);
        txtIdEstadoCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdEstadoCuentaActionPerformed(evt);
            }
        });

        jLabel9.setText("MORA");

        txtMora.setText("0.00");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtIdCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtIdCredito, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtIdEstadoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtCuota, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtMora, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnRegistroDep)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCancelarDep)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtIdCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtIdCredito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtIdEstadoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCuota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRegistroDep)
                    .addComponent(btnCancelarDep, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/consultas111100.gif"))); // NOI18N
        jLabel6.setText("BUSCAR");

        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField1KeyReleased(evt);
            }
        });

        jTable1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/pdf1.gif"))); // NOI18N
        jButton1.setText("Generar reporte");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setBackground(new java.awt.Color(255, 255, 255));
        jButton2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/imprimir0022.gif"))); // NOI18N
        jButton2.setText("Imprimir recibo");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jButton1)
                                .addComponent(jButton2)
                                .addComponent(jLabel6)))
                        .addGap(0, 15, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRegistroDepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistroDepActionPerformed
        
        String identificacion,cuota,idCredito,idEstadoC,mora;
       
        if(this.txtIdCliente.getText().isEmpty()||
           this.txtCuota.getText().isEmpty()||
           this.txtIdEstadoCuenta.getText().isEmpty()||
           this.txtMora.getText().isEmpty()){
            JOptionPane.showMessageDialog(this,"PORFAVOR RELLENE TODOS LOS CAMPOS");
            
            return;
        }
        identificacion= txtIdCliente.getText();
        idEstadoC=txtIdEstadoCuenta.getText();
        cuota =txtCuota.getText();
        idCredito=txtIdCredito.getText();
        mora=txtMora.getText();
        
        try{
            
            
             CallableStatement statement=(CallableStatement) cd.prepareCall("{call registrarDepositoLiquidacion(?,?,?,?,?)}");
             statement.setString("inIdCliente",identificacion);
             statement.setString("inIdCredito",idCredito);
             statement.setString("inIdEstado",idEstadoC);
             statement.setString("inCantidad",cuota);
             statement.setString("inMora",mora);         
           
            int  n= statement.executeUpdate();
                         
             if(n>0){
                 JOptionPane.showMessageDialog(null,"Deposito realizado Correctamente");
                 
                 limpiar();
                 cargar("");
                 jTextField1.requestFocus();
             }
        }
        catch(SQLException ex){
            
            JOptionPane.showMessageDialog(null,"Error DEPOSITO NO REALIZADO",null,0);
        }   
    }//GEN-LAST:event_btnRegistroDepActionPerformed

    private void btnCancelarDepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarDepActionPerformed
        
        limpiar();
    }//GEN-LAST:event_btnCancelarDepActionPerformed

    private void jTextField1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyReleased
        cargar(jTextField1.getText());
    }//GEN-LAST:event_jTextField1KeyReleased

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try{
            
          String ubicacionReportes=System.getProperty("user.dir")+"/src/reportes/imprimirReciboDescuento.jasper";
            
            JasperReport reporteJasper= (JasperReport)JRLoader.loadObject(ubicacionReportes);
            
            Map parametro =new HashMap();
            parametro.put("idCliente",txtIdCliente.getText());
            parametro.put("usuario",lblUsuario.getText());
            parametro.put("idEstado",txtIdEstadoCuenta.getText());
            
            JasperPrint mostrarReporte= JasperFillManager.fillReport(reporteJasper,parametro,cd);
            JasperViewer view = new JasperViewer(mostrarReporte,false);
            view.setVisible(true);
            
        
        }
         catch(Exception ex){
           JOptionPane.showMessageDialog(this,ex.getMessage());
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtIdCreditoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdCreditoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdCreditoActionPerformed

    private void txtIdEstadoCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdEstadoCuentaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdEstadoCuentaActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        try{
            
          String ubicacionReportes=System.getProperty("user.dir")+"/src/reportes/imprimirReciboDescuento.jasper";
            
            JasperReport reporteJasper= (JasperReport)JRLoader.loadObject(ubicacionReportes);
            
            Map parametro =new HashMap();
            parametro.put("idCliente",txtIdCliente.getText());
            parametro.put("usuario",lblUsuario.getText());
            parametro.put("idEstado",txtIdEstadoCuenta.getText());
            
            JasperPrint mostrarReporte= JasperFillManager.fillReport(reporteJasper,parametro,cd);
            JasperPrintManager.printReport(mostrarReporte,false);//true para buscar impresora
       
        }
         catch(Exception ex){
           JOptionPane.showMessageDialog(this,ex.getMessage());
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelarDep;
    private javax.swing.JButton btnRegistroDep;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JTextField txtCuota;
    private javax.swing.JTextField txtIdCliente;
    private javax.swing.JTextField txtIdCredito;
    private javax.swing.JTextField txtIdEstadoCuenta;
    private javax.swing.JTextField txtMora;
    // End of variables declaration//GEN-END:variables
}
