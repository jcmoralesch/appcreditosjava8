
package formulario;

import com.mysql.jdbc.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;


public class ConsultarActivarInactivos extends javax.swing.JInternalFrame {
      DefaultTableModel model;
      int contador;
       private static CallableStatement cs=null;
       private static PreparedStatement ps= null;
       private static Statement st =null;
       ResultSet rs =null;
        
        ConexionFR cn = new ConexionFR();
        Connection cd = cn.Conexion();

      
    public ConsultarActivarInactivos() {
        initComponents();
         cargar("");
         metodoTipoPagoCliete();
         metodoTipoCredito1();
         bloquerCredito();
         limpiarCredito();
         
         jTable1.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
        
          public void valueChanged(ListSelectionEvent e){
              if(jTable1.getSelectedRow()!=-1){
                    int fila = jTable1.getSelectedRow();
                    txtIdCredito.setText(jTable1.getValueAt(fila,0).toString());
              }
       }
       
    });
         
    }
    
     
     void limpiarCredito(){
         txtIdCredito.setText("");
         txtCreTipo.setText("");
         txtTipoCredito1.setText("");
    }
    
    void bloquerCredito(){
        
      txtIdCredito.setEditable(false);
      txtCreTipo.setEditable(false);
      btnCreReg.setEnabled(false);
      btnCreCan.setEnabled(false);
      jButton1.setEnabled(true);
    
    }
    
     void desbloquearCredito(){
       txtIdCredito.setEditable(true);
       txtCreTipo.setEditable(true);
      btnCreReg.setEnabled(true);
      btnCreCan.setEnabled(true);
      jButton1.setEnabled(false);
    }
    
    
    
    void cargar(String valor){
      String [] titulos={"IDENTIFICACION","ID. EMPLEADO","NOMBRE","APELLIDO","DIRECCION","TELEFONO","FECHA DE REGISTRO"}; 
      String [] registros =new String[7];
      
      String sql="select B.identificacion,B.emp_identificacion,B.nombre_cliente,B.ape_cliente,B.direccion,B.telefono,B.status,B.fecha\n" +
"                  from Cliente B\n" +
"                  where CONCAT(nombre_cliente,'',ape_cliente) like'%"+valor+"%';";
      
      String total="select count(identificacion)from cliente where status='I'";
      
      model = new DefaultTableModel(null,titulos);
      
      Statement st;
       try {
           st = cd.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
                while(rs.next()){
                
                       registros[0]=rs.getString("identificacion");
                       registros[1]=rs.getString("emp_identificacion");
                       registros[2]=rs.getString("nombre_cliente");
                       registros[3]=rs.getString("ape_cliente");
                       registros[4]=rs.getString("direccion");
                       registros[5]=rs.getString("telefono");
                       registros[6]=rs.getString("fecha");
                      
                       model.addRow(registros);
                }
                
                jTable1.setModel(model);
       } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, ex);
       }
     
  }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        txtAux = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtIdCredito = new javax.swing.JTextField();
        btnCreReg = new javax.swing.JButton();
        btnCreCan = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        txtCreTipo = new javax.swing.JTextField();
        jComboBox3 = new javax.swing.JComboBox();
        jLabel19 = new javax.swing.JLabel();
        txtTipoCredito1 = new javax.swing.JTextField();
        jComboBox5 = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);

        jPanel1.setBackground(new java.awt.Color(0, 51, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("SELECIONE UN CLIENTE PARA OTORGAR CREDITO ");

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/recortado2.gif"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel3)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(0, 204, 255));
        jPanel4.setPreferredSize(new java.awt.Dimension(1143, 596));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setText("BUSCAR");

        txtAux.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtAuxKeyReleased(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(0, 204, 0)), "OTORGAR CREDITO A CLIENTE REGISTRADO", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/creditos6.gif"))); // NOI18N
        jLabel13.setBorder(new javax.swing.border.MatteBorder(null));

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel14.setText("Identificacion");

        txtIdCredito.setEditable(false);

        btnCreReg.setBackground(new java.awt.Color(255, 255, 255));
        btnCreReg.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnCreReg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/creditos613.gif"))); // NOI18N
        btnCreReg.setText("Registrar");
        btnCreReg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreRegActionPerformed(evt);
            }
        });

        btnCreCan.setBackground(new java.awt.Color(255, 255, 255));
        btnCreCan.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnCreCan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/canelar181.gif"))); // NOI18N
        btnCreCan.setText("Cancelar");
        btnCreCan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreCanActionPerformed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel15.setText("Tipo de Pago");

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox3ActionPerformed(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel19.setText("Tipo credito");

        jComboBox5.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBox5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox5ActionPerformed(evt);
            }
        });

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/nuevo31.gif"))); // NOI18N
        jButton1.setText("Nuevo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setBackground(new java.awt.Color(255, 255, 255));
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/reporteGeren.gif"))); // NOI18N
        jButton2.setText("Calendario");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCreReg)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCreCan)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtIdCredito, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(3, 3, 3)
                                        .addComponent(jLabel15)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtCreTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(jLabel19)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(txtTipoCredito1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(15, 15, 15)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jComboBox5, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txtIdCredito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(txtTipoCredito1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(txtCreTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCreReg)
                    .addComponent(btnCreCan)
                    .addComponent(jButton1)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 870, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 5, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtAux, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16))))
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addGap(548, 548, 548)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(288, Short.MAX_VALUE)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(txtAux, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addGap(287, 287, 287)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(226, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 885, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 535, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtAuxKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAuxKeyReleased
        cargar(txtAux.getText());
    }//GEN-LAST:event_txtAuxKeyReleased
    
    private int consultarNoPago(String identificacion){
         
       int noPagos=0;
       try{ 
       String sql="select no_pago from tipo_pago where id_tipo='"+identificacion+"'";
                
             ps =cd.prepareStatement(sql);
             rs=ps.executeQuery();
             rs.next();
             noPagos=rs.getInt("no_pago");
       }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"");
        }
       return noPagos;
    }
    
       public static String existeCredito(String cliente){
        
                  ConexionFR cn = new ConexionFR();
                  Connection cd = cn.Conexion();
                  
                String sql="call creditoActivo(?,?)";
                String clienteEncontrado=null;
                
                try{
                        cs = (CallableStatement) cd.prepareCall(sql);
                        cs.setString(1,cliente);
                        cs.registerOutParameter(2,java.sql.Types.VARCHAR);
                        cs.execute();
                        clienteEncontrado = cs.getString(2);
                }
                catch(SQLException ex){
                   ex.printStackTrace();
                }
                System.out.println(sql);
                return clienteEncontrado;
                
    }
    
    
    private void btnCreRegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreRegActionPerformed
       
        String idCliente,idTipo,fechaVen,tipoCredito;
        if(this.txtIdCredito.getText().isEmpty()||
            this.txtCreTipo.getText().isEmpty()){
            JOptionPane.showMessageDialog(this,"POR FAVOR RELLENE TODOS LOS CAMPOS");
            return;
        }
        
        idCliente= txtIdCredito.getText();
        String creditoExistente =existeCredito(idCliente);
        if(creditoExistente!=null){
            JOptionPane.showMessageDialog(this,"EL CLIENTE "+this.txtIdCredito.getText()+" TIENE CREDITO ASIGNADO\nACTIVELO PARA PODER ASIGNAR CREDITO"
                + "\n cancele su credito para poder otorgar credito");
            return;
        }
        idCliente= txtIdCredito.getText();
        idTipo=txtCreTipo.getText();
        tipoCredito=txtTipoCredito1.getText();

        try{

            CallableStatement statement=(CallableStatement) cd.prepareCall("{call otorgarCredito(?,?,?)}");
            statement.setString("inIdCliente",idCliente);
            statement.setString("inIdCredito",tipoCredito);
            statement.setString("inIdtipo",idTipo);

            int n= statement.executeUpdate();

            if(n>0){
                JOptionPane.showMessageDialog(null,"Datos ingresados correctamente");
                String fecha="";
                if(tipoCredito.equals("2")){

                    int noPagos=consultarNoPago(idTipo);

                    for(int i=1;i<=noPagos;i++){
                        fecha=JOptionPane.showInputDialog(rootPane,"Ingrese fecha de pago "+ i +" "+ fecha,fecha);

                        try{
                            CallableStatement statement1=(CallableStatement) cd.prepareCall("{call registrarFechasPago(?,?,?)}");
                            statement1.setString("inIdentificacion",idCliente);
                            statement1.setString("inFechaPago",fecha);
                            statement1.setString("inTipoCredito",tipoCredito);

                            statement1.executeUpdate();

                            if(i==noPagos){
                                CallableStatement statement2=(CallableStatement) cd.prepareCall("{call actualizarCredito(?,?)}");
                                statement2.setString("inIdentificacion",idCliente);
                                statement2.setString("inFecha",fecha);                

                                statement2.executeUpdate();
                            }
                        }catch(Exception ex){
                            JOptionPane.showMessageDialog(null,"Error DATOS NO INGRESADOS");
                        }

                    }
                    JOptionPane.showMessageDialog(null,"Fechas ingresadas correctamente");
                }

                else{
                    fecha=JOptionPane.showInputDialog(rootPane,"Ingrese fecha de vencimiento ");

                    try{
                        CallableStatement statement2=(CallableStatement) cd.prepareCall("{call actualizarCredito(?,?)}");
                        statement2.setString("inIdentificacion",idCliente);
                        statement2.setString("inFecha",fecha);

                        statement2.executeUpdate();
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null,"Error FECHA NO INGRESADA");
                    }
                    JOptionPane.showMessageDialog(null,"Fecha ingresada correctamente");
                }
                bloquerCredito();
                // cargar("");
            }
        }
        catch(Exception ex){
            //Logger.getLogger(Ing_Clie.class.getName()).log(Level.SEVERE,sql);
            JOptionPane.showMessageDialog(null,"Error DATOS NO INGRESADOS");
        }
    }//GEN-LAST:event_btnCreRegActionPerformed

    private void btnCreCanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreCanActionPerformed
        bloquerCredito();
        limpiarCredito();
    }//GEN-LAST:event_btnCreCanActionPerformed

    private void jComboBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox3ActionPerformed
        try{
            Statement sent1= cd.createStatement();
            ResultSet rsl = sent1.executeQuery("select * from tipo_pago  where nombre_tipo='"+this.jComboBox3.getSelectedItem()+"'");
            rsl.next();
            txtCreTipo.setText(String.valueOf(rsl.getString("id_tipo")));
        }
        catch(SQLException e){
            // JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_jComboBox3ActionPerformed

    private void jComboBox5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox5ActionPerformed
        // TODO add your handling code here:
        try{
            Statement sent1= cd.createStatement();
            ResultSet rsl = sent1.executeQuery("select * from tipo_credito where  tipo='"+this.jComboBox5.getSelectedItem()+"'");
            rsl.next();
            txtTipoCredito1.setText(String.valueOf(rsl.getString("id_Tipo")));
        }
        catch(SQLException e){
            // JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_jComboBox5ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        desbloquearCredito();
        limpiarCredito();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        try{
            
          String ubicacionReportes=System.getProperty("user.dir")+"/src/reportes/fechaPagos.jasper";
            
            JasperReport reporteJasper= (JasperReport)JRLoader.loadObject(ubicacionReportes);
            Map campo= new HashMap();
            campo.put("idCliente",txtIdCredito.getText());
            
            JasperPrint mostrarReporte= JasperFillManager.fillReport(reporteJasper,campo,cd);
            JasperViewer view = new JasperViewer(mostrarReporte,false);
            view.setVisible(true);
            
        
        }
        catch(Exception ex){
           JOptionPane.showMessageDialog(this,ex.getMessage());
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void metodoTipoCredito1(){
    this.jComboBox5.removeAllItems();
        try{
            
            Statement sent =cd.createStatement();
            ResultSet rs=sent.executeQuery("select * from tipo_credito where tipo='Semanal' or tipo='Diario'");
            while(rs.next()){
                 this.jComboBox5.addItem(rs.getString("tipo"));
            }
              contador++;    
        }catch(SQLException e){
             JOptionPane.showMessageDialog(null,e);
        }
} 
    
    private void metodoTipoPagoCliete(){
       this.jComboBox3.removeAllItems();
        try{
            
            Statement sent =cd.createStatement();
            ResultSet rs=sent.executeQuery("select * from tipo_pago where id_tipo2=1 or id_tipo2=2");
            while(rs.next()){
                 this.jComboBox3.addItem(rs.getString("nombre_tipo"));
            }
              contador++;    
        }catch(SQLException e){
             JOptionPane.showMessageDialog(null,e);
        }


}

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCreCan;
    private javax.swing.JButton btnCreReg;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox jComboBox3;
    private javax.swing.JComboBox jComboBox5;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtAux;
    private javax.swing.JTextField txtCreTipo;
    private javax.swing.JTextField txtIdCredito;
    private javax.swing.JTextField txtTipoCredito1;
    // End of variables declaration//GEN-END:variables
}


