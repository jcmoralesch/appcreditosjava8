package formulario;

import com.mysql.jdbc.Connection;
import java.sql.CallableStatement;
import java.sql.ResultSet;


public class DatosUsuario {
    
    

public static TUsuario GetByUsuarioContrasenia(Connection conexion,String usuario, String contrasenia) throws Exception{
       TUsuario tUsuario = null;
      
       try{
         CallableStatement statement =(CallableStatement) conexion.prepareCall("{call procUsuarioContra(?,?)}");
        
         statement.setString("inUsuario", usuario);
         statement.setString("inContrasenia",contrasenia);
         
         ResultSet resultSet=statement.executeQuery();
         if(resultSet.next()){
             tUsuario= new TUsuario(
                     
                     resultSet.getInt("id_usuario"),
                     resultSet.getString("identificacion"),
                     resultSet.getString("usuario"),
                     resultSet.getString("password"));
         }
       }catch(Exception ex){
           throw new Exception(ex.getMessage());
       }
       finally{
          return tUsuario;
       }
     
    
    }

}
