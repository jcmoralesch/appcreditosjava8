

package formulario;

import com.mysql.jdbc.Connection;
import javax.swing.JOptionPane;


public class FrmLogin {
    public static boolean login(String usuario,String contrasenia){
             boolean existeUsuario= false;
           //  Connection conexion=Conexion();
             ConexionFR con = new ConexionFR();
             Connection conexion=con.Conexion();
             
             try{
                 conexion.setAutoCommit(false);
                 TUsuario tUsuario=DatosUsuario.GetByUsuarioContrasenia(conexion,usuario,contrasenia);
                 if(tUsuario!=null){
                     existeUsuario= true;
                 }
                 conexion.commit();
             }
             catch(Exception ex){
                 conexion.rollback();
                 JOptionPane.showMessageDialog(null,ex.getMessage());
             }
             finally{
                return existeUsuario;
             }
     }
}
