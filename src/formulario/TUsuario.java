
package formulario;


public class TUsuario {
    private int idUsuario;
    private String identificacion;
    private String usuario;
    private String password;
   
    
    public TUsuario(){}
    
    public TUsuario(int idUsuario,String identificacion, String usuario, String password ){
        this.idUsuario=idUsuario;
        this.identificacion=identificacion;
        this.usuario=usuario;
        this.password=password;
       
    }
    
    public void idUsuario(int idUsuario){
       this.idUsuario=idUsuario;
    }
    
    public int idUsario(){
         return this.idUsuario;
         
    }
    
      public void identificacion(String identificacion){
         this.identificacion=identificacion;
    }
     public String identificacion(){
         return this.identificacion;
    }
    
    public void usuario(String usuario){
       this.usuario=usuario;
    }
    
    public String usuario(){
         return this.usuario;
    }
    
     public void password(String password){
         this.password=password;
    }
     public String password(){
         return this.password;
    }
     
}
