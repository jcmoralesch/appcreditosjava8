
package formulario;
import com.mysql.jdbc.CallableStatement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class ActualizarCreditoPrendario extends javax.swing.JInternalFrame {

     int contador;
     DefaultTableModel model;
     private static PreparedStatement ps= null;
     ResultSet rs =null;
    
     ConexionFR cn = new ConexionFR();
     Connection cd = cn.Conexion();
    
    public ActualizarCreditoPrendario() {
        initComponents();
        limpiar();
        cargar("");
        bloquear();
        metodoTipoCredito1();
        metodoTipoPagoCliete();
        
       jTable1.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
        
        
        public void valueChanged(ListSelectionEvent e){
              if(jTable1.getSelectedRow()!=-1){
                    int fila = jTable1.getSelectedRow();
                    txtIdCredito.setText(jTable1.getValueAt(fila,0).toString());
                    txtIdentificacion.setText(jTable1.getValueAt(fila,1).toString());
                    txtCantOtorgada.setText(jTable1.getValueAt(fila,6).toString());
                    txtFecha.setText(jTable1.getValueAt(fila,8).toString());
                    txtIdEstadoCuenta.setText(jTable1.getValueAt(fila,9).toString());
              }
       }
    });
    }

    
     void limpiar(){
      
      txtTipoCredito1.setText("");
      txtCreTipo.setText("");
      txtFecha.setText("DD/MM/AAAA");
      txtIdCredito.setText("");
      txtIdEstadoCuenta.setText("");
     }         
       
     void bloquear(){
      btnCreReg.setEnabled(false);
      btnCreCan.setEnabled(false);
      jButton3.setEnabled(true);
    
    }
    
   void desbloquear(){
      btnCreReg.setEnabled(true);
      btnCreCan.setEnabled(true);
      jButton3.setEnabled(false);
    }
       
    void cargar(String valor){
      String [] titulos={"Id. Cred","Identificacion","Nombre","Apellido","Tipo credito","Credito","Monto","Monto total","Fecha venci","Id. Estado cuenta"}; 
      String [] registros =new String[10];
      
      String sql="  select C.id_credito,C.identificacion,B.nombre_cliente,B.ape_cliente,T.nombre_tipo,C.monto,C.monto_total,date_format(C.fecha_venci,'%d/%m/%Y'),R.tipo,\n" +
"                 E.id_estado\n" +
"                 from credito_prendario C, estado_cuenta E,tipo_pago_prendaria T, cliente B, tipo_credito R \n" +
"                 where C.identificacion=E.identificacion and C.identificacion=B.identificacion and C.id_tipo_pago=T.id_tipo_pago and C.id_tipo=R.id_tipo and C.status='P' \n" +
"               and E.status='P' and E.pagos_realizados=0 and CONCAT(B.nombre_cliente,'',B.ape_cliente) like'%"+valor+"%'";
      model = new DefaultTableModel(null,titulos);
      
      ConexionFR cc = new ConexionFR();
      Connection cn = cc.Conexion();
      
      Statement st;
       try {
           st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
                while(rs.next()){
                       registros[0]=rs.getString("id_credito");
                       registros[1]=rs.getString("identificacion");
                       registros[2]=rs.getString("nombre_cliente");
                       registros[3]=rs.getString("ape_cliente");
                       registros[4]=rs.getString("nombre_tipo");
                       registros[5]=rs.getString("tipo");
                       registros[6]=rs.getString("monto");
                       registros[7]=rs.getString("monto_total");
                       registros[8]=rs.getString("date_format(C.fecha_venci,'%d/%m/%Y')");
                       registros[9]=rs.getString("id_estado");
                      
                       model.addRow(registros);
                }
                
                jTable1.setModel(model);
       } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, ex);
       }
     
  }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        btnCreReg = new javax.swing.JButton();
        btnCreCan = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        txtCreTipo = new javax.swing.JTextField();
        jComboBox3 = new javax.swing.JComboBox();
        jLabel19 = new javax.swing.JLabel();
        txtTipoCredito1 = new javax.swing.JTextField();
        jComboBox5 = new javax.swing.JComboBox();
        jButton3 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtFecha = new javax.swing.JTextField();
        txtIdCredito = new javax.swing.JTextField();
        txtIdEstadoCuenta = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtCantOtorgada = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtIdentificacion = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);

        jPanel1.setBackground(new java.awt.Color(0, 51, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "AREA DE ACTULIZACION DE CREDITO", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11), new java.awt.Color(255, 255, 255))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/recortado2.gif"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel2))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 90, Short.MAX_VALUE)
                .addGap(1, 1, 1))
        );

        jPanel3.setBackground(new java.awt.Color(0, 204, 255));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/consultas111100.gif"))); // NOI18N
        jLabel6.setText("BUSCAR");

        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField1KeyReleased(evt);
            }
        });

        jTable1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/reporteGeren.gif"))); // NOI18N
        jButton1.setText("Calendario");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(0, 204, 0)), "ACTUALIZAR DATOS DE CREDITO ", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        btnCreReg.setBackground(new java.awt.Color(255, 255, 255));
        btnCreReg.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnCreReg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/actualizacionjpg.gif"))); // NOI18N
        btnCreReg.setText("Actualizar");
        btnCreReg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreRegActionPerformed(evt);
            }
        });

        btnCreCan.setBackground(new java.awt.Color(255, 255, 255));
        btnCreCan.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnCreCan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/canelar181.gif"))); // NOI18N
        btnCreCan.setText("Cancelar");
        btnCreCan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreCanActionPerformed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel15.setText("Tipo de pago");

        txtCreTipo.setEditable(false);

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox3ActionPerformed(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel19.setText("Tipo crédito");

        txtTipoCredito1.setEditable(false);

        jComboBox5.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBox5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox5ActionPerformed(evt);
            }
        });

        jButton3.setBackground(new java.awt.Color(255, 255, 255));
        jButton3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/nuevo31.gif"))); // NOI18N
        jButton3.setText("Nuevo");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Fecha vencimiento");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Id. Crédito");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Id. Estado cuenta");

        txtFecha.setText("DD/MM/AAAA");

        txtIdCredito.setEditable(false);

        txtIdEstadoCuenta.setEditable(false);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Cant. Otorgada");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel7.setText("Identificación");

        txtIdentificacion.setEditable(false);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCreReg)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCreCan))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel1)
                                            .addComponent(jLabel15))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel4Layout.createSequentialGroup()
                                                .addComponent(txtCreTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel4Layout.createSequentialGroup()
                                                .addComponent(txtTipoCredito1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jComboBox5, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(txtIdentificacion, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel3)
                                            .addComponent(jLabel4)
                                            .addComponent(jLabel5))
                                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel4Layout.createSequentialGroup()
                                                .addGap(12, 12, 12)
                                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(txtIdCredito, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(txtCantOtorgada, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                            .addGroup(jPanel4Layout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(txtIdEstadoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                            .addComponent(jLabel19)
                            .addComponent(jLabel7))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(23, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtIdentificacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(txtTipoCredito1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(txtCreTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtCantOtorgada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtIdCredito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtIdEstadoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCreReg)
                    .addComponent(btnCreCan)
                    .addComponent(jButton3))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 991, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(246, 246, 246)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(1, 1, 1))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyReleased
        cargar(jTextField1.getText());
    }//GEN-LAST:event_jTextField1KeyReleased
    
    
    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try{
            
          String ubicacionReportes=System.getProperty("user.dir")+"/src/reportes/UfechaPagos.jasper";
            
            JasperReport reporteJasper= (JasperReport)JRLoader.loadObject(ubicacionReportes);
            Map campo= new HashMap();
            campo.put("idCliente",txtIdentificacion.getText());
            
            JasperPrint mostrarReporte= JasperFillManager.fillReport(reporteJasper,campo,cd);
            JasperViewer view = new JasperViewer(mostrarReporte,false);
            view.setVisible(true);
            
        
        }
        catch(Exception ex){
           JOptionPane.showMessageDialog(this,ex.getMessage());
        }
    }//GEN-LAST:event_jButton1ActionPerformed

     private int consultarNoPago(String identificacion){
       int noPagos=0;
       try{ 
       String sql="select no_pago from tipo_pago_prendaria where id_tipo_pago='"+identificacion+"'";
                
             ps =cd.prepareStatement(sql);
             rs=ps.executeQuery();
             rs.next();
             noPagos=rs.getInt("no_pago");
       }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"");
        }
       return noPagos;
    }
    
     private int consultarTipoCredito(String idCredito){
         
       int tipoCredito=0;
       try{ 
       String sql="select id_tipo from credito_prendario where id_credito='"+idCredito+"'";
                
             ps =cd.prepareStatement(sql);
             rs=ps.executeQuery();
             rs.next();
             tipoCredito=rs.getInt("id_tipo");
       }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"");
        }
       return tipoCredito;
    }
    
    private void btnCreRegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreRegActionPerformed

        
        String tipoCredito,tipoPago,fecha,idCredito,idEstado,cantidad,identificacion;
        
        if(txtIdCredito.getText().isEmpty()||
           txtCreTipo.getText().isEmpty()||
           txtTipoCredito1.getText().isEmpty()||
           txtFecha.getText().isEmpty()||
           txtIdEstadoCuenta.getText().isEmpty()||
           txtCantOtorgada.getText().isEmpty()||
           txtIdentificacion.getText().isEmpty()){
            JOptionPane.showMessageDialog(this,"POR FAVOR RELLENE TODOS LOS CAMPOS");
            return;
        }

        tipoCredito=txtTipoCredito1.getText();
        tipoPago=txtCreTipo.getText();
        fecha=txtFecha.getText();
        idCredito=txtIdCredito.getText();
        idEstado=txtIdEstadoCuenta.getText();
        cantidad=txtCantOtorgada.getText();
        identificacion=txtIdentificacion.getText();
        
        int consultaCredito=consultarTipoCredito(idCredito);

        try{

            CallableStatement statement=(CallableStatement) cd.prepareCall("{call actualizarDatosCreditoPre(?,?,?,?,?,?)}");
            statement.setString("inTipoCredito",tipoCredito);
            statement.setString("inTipoPago",tipoPago);
            statement.setString("inFecha",fecha);
            statement.setString("inCantidad",cantidad);
            statement.setString("inIdCredito",idCredito);
            statement.setString("inIdEstado",idEstado);

            int n= statement.executeUpdate();

            if(n>0){
                
                if(consultaCredito==3 && tipoCredito.equals("3")){
                    JOptionPane.showMessageDialog(null,"Datos actualizados correctamente");  
                
                    bloquear();
                    cargar("");
                }
                else if(consultaCredito==3 && tipoCredito.equals("4")){
                   String fechas="";
                   
                   int noPagos=consultarNoPago(tipoPago);

                           try{
                               CallableStatement statement1=(CallableStatement) cd.prepareCall("{call fechasSemanales(?,?,?)}");
                               statement1.setString("inIdentificacion",identificacion);
                               statement1.setInt("inNoPagos",noPagos);
                               statement1.setString("inTipoCredito",tipoCredito);

                               statement1.executeUpdate();
                         }catch(Exception ex){
                              JOptionPane.showMessageDialog(null,"Error DATOS NO INGRESADOS");
                         } 
                   
                   JOptionPane.showMessageDialog(null,"Fechas Actualizadas correctamente correctamente");
                   JOptionPane.showMessageDialog(null,"DATOS ACTUALIZADOS CORRECTAMENTE");  
                        
                      bloquear();
                      cargar("");
                }        
                
                else if(consultaCredito==4 && tipoCredito.equals("4")){
                    int noPagos=consultarNoPago(tipoPago);
                    String tipoC= Integer.toString(consultaCredito);
                    
                    try{ 
                       CallableStatement statement3=(CallableStatement) cd.prepareCall("{call borrarFechas(?,?)}");
                       statement3.setString("inIdentificacion",identificacion);
                       statement3.setString("inTipoCredito",tipoC);
                       statement3.executeUpdate();
                       
                       CallableStatement statement1=(CallableStatement) cd.prepareCall("{call fechasSemanales(?,?,?)}");
                               statement1.setString("inIdentificacion",identificacion);
                               statement1.setInt("inNoPagos",noPagos);
                               statement1.setString("inTipoCredito",tipoCredito);

                               statement1.executeUpdate();
                       
                   }catch(Exception ex){
                              JOptionPane.showMessageDialog(null,"Error FECHA NO INGRESADA");
                         }    
                    
                      JOptionPane.showMessageDialog(null,"Datos actualizados correctamente");  
                
                      bloquear();
                      cargar("");
                }
                
                else if(consultaCredito==4 && tipoCredito.equals("3")){
                    
                       String tipoC= Integer.toString(consultaCredito);
                       int noPagos=consultarNoPago(tipoPago);

                           try{
                               CallableStatement statement3=(CallableStatement) cd.prepareCall("{call borrarFechas(?,?)}");
                               statement3.setString("inIdentificacion",identificacion);
                               statement3.setString("inTipoCredito",tipoC);
                               statement3.executeUpdate();
                               
                               CallableStatement statement2=(CallableStatement) cd.prepareCall("{call REGISTRO_FECHAS_PAGO(?,?,?)}");
                               statement2.setString("inIdentificacion",identificacion);
                               statement2.setInt("inNoPagos",noPagos);
                               statement2.setString("inTipoCredito",tipoCredito);
                               statement2.executeUpdate();
                               
                               
                           }catch(Exception ex){
                              JOptionPane.showMessageDialog(null,"Error FECHA NO INGRESADA");
                         }
                       JOptionPane.showMessageDialog(null,"Fecha ingresada correctamente");
                       
                       JOptionPane.showMessageDialog(null,"Datos actualizados correctamente");              
                       bloquear();
                       cargar("");
                }
            }
        }
        catch(Exception ex){
            //Logger.getLogger(Ing_Clie.class.getName()).log(Level.SEVERE,sql);
            JOptionPane.showMessageDialog(null,"Error DATOS NO Actualizados");
        }
    }//GEN-LAST:event_btnCreRegActionPerformed

    private void btnCreCanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreCanActionPerformed
        bloquear();
        limpiar();
    }//GEN-LAST:event_btnCreCanActionPerformed

    private void jComboBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox3ActionPerformed
        try{
            Statement sent1= cd.createStatement();
            ResultSet rsl = sent1.executeQuery("select * from tipo_pago_prendaria  where nombre_tipo='"+this.jComboBox3.getSelectedItem()+"'");
            rsl.next();
            txtCreTipo.setText(String.valueOf(rsl.getString("id_tipo_pago")));
        }
        catch(SQLException e){
            // JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_jComboBox3ActionPerformed

    private void jComboBox5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox5ActionPerformed
        // TODO add your handling code here:
         try{
            Statement sent1= cd.createStatement();
            ResultSet rsl = sent1.executeQuery("select * from tipo_credito where  tipo='"+this.jComboBox5.getSelectedItem()+"'");
            rsl.next();
            txtTipoCredito1.setText(String.valueOf(rsl.getString("id_Tipo")));
        }
        catch(SQLException e){
            // JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_jComboBox5ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        desbloquear();
        limpiar();
    }//GEN-LAST:event_jButton3ActionPerformed
 
    
     private void metodoTipoPagoCliete(){
       this.jComboBox3.removeAllItems();
        try{
            
            Statement sent =cd.createStatement();
            ResultSet rs=sent.executeQuery("select * from tipo_pago_prendaria");
            while(rs.next()){
                 this.jComboBox3.addItem(rs.getString("nombre_tipo"));
            }
              contador++;    
        }catch(SQLException e){
             JOptionPane.showMessageDialog(null,e);
        }


}
     
         private void metodoTipoCredito1(){
    this.jComboBox5.removeAllItems();
        try{
            
            Statement sent =cd.createStatement();
            ResultSet rs=sent.executeQuery("select * from tipo_credito where tipo='Prendario semanal' or tipo='Prendario diario'");
            while(rs.next()){
                 this.jComboBox5.addItem(rs.getString("tipo"));
            }
              contador++;    
        }catch(SQLException e){
             JOptionPane.showMessageDialog(null,e);
        }
}
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCreCan;
    private javax.swing.JButton btnCreReg;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JComboBox jComboBox3;
    private javax.swing.JComboBox jComboBox5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField txtCantOtorgada;
    private javax.swing.JTextField txtCreTipo;
    private javax.swing.JTextField txtFecha;
    private javax.swing.JTextField txtIdCredito;
    private javax.swing.JTextField txtIdEstadoCuenta;
    private javax.swing.JTextField txtIdentificacion;
    private javax.swing.JTextField txtTipoCredito1;
    // End of variables declaration//GEN-END:variables
}
